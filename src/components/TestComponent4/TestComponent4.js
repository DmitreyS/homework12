import Button from '@mui/material/Button';
import Avatar from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { deepOrange, deepPurple } from '@mui/material/colors';
import TextField from '@mui/material/TextField';
import './TestComponent4.scss';

function TestComponent4() {
    return (
        <div>
            <h1>I'm fourth MUI test Component</h1>

            <Stack direction="row" spacing={2}>
                <Avatar>fourth</Avatar>
                <Avatar sx={{ bgcolor: deepOrange[500] }}>Number</Avatar>
                <Avatar sx={{ bgcolor: deepPurple[500] }}>Component4</Avatar>
            </Stack>

            <div className={'knopka'}>
            <Button variant="contained" color="success">Number Four</Button>
            </div>

            <div className={'field'}>
                <TextField id="standard-basic" label="Введите email" variant="standard" />
            </div>

        </div>
    );
}

export default TestComponent4;