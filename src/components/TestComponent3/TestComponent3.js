import styled from 'styled-components'

const MyStyledDiv = styled.div`
color: blue;
font-size: 28px;
`

function TestComponent3() {
    return (
        <MyStyledDiv>I'm third styled-components test Component</MyStyledDiv>
    );
}

export default TestComponent3;