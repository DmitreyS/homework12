const divStyle = {
    color: 'yellow',
    fontSize: '60px',
}

function TestComponent5() {
    return (
        <div style={divStyle}>I'm an inline style fifth test Component</div>
    );
}

export default TestComponent5;