import styles from './TestComponent2.module.scss';


function TestComponent2() {
    return (
        <div className={styles.text}>I'm second module scss test Component</div>
    );
}

export default TestComponent2;