import './App.scss';
import TestComponent1 from './components/TestComponent1/TestComponent1';
import TestComponent2 from './components/TestComponent2/TestComponent2';
import TestComponent3 from './components/TestComponent3/TestComponent3';
import TestComponent4 from './components/TestComponent4/TestComponent4';
import TestComponent5 from './components/TestComponent5/TestComponent5';

function App() {
  return (
    <div className="App">
      <TestComponent1></TestComponent1>
      <TestComponent2></TestComponent2>
      <TestComponent3></TestComponent3>
      <TestComponent4></TestComponent4>
      <TestComponent5></TestComponent5>

    </div>
  );
}

export default App;
